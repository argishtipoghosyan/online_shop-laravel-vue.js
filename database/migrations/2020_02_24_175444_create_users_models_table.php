<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersModelsTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',255);
            $table->string('surname',255);
            $table->bigInteger('age');
            $table->string('email',255)->unique();
            $table->string('password',255);
            $table->string('status',255)->default('user');
            $table->bigInteger('active')->default(0);
            $table->bigInteger('code')->default(0);
            $table->string('time',255)->default(0);

            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_models');
    }
}
