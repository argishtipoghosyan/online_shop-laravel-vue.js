<!--
Author: Colorlib
Author URL: https://colorlib.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Creative Colorlib SignUp Form</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="{{URL::asset('css/signup.css')}}" rel="stylesheet" type="text/css" media="all" />
<!-- //Custom Theme files -->
<!-- web font -->
<link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
<!-- //web font -->
</head>
<body>
	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>Log In</h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				<form action="{{URL::to('/restore')}}" method="post">
					{{csrf_field()}}
				@if(!Session::has('ka'))
					@if($errors->has('email1'))
                       <p class="errors"> {{$errors->first('email1')}}</p>
					@endif
					@if(Session::has('emmail1'))
                       <p class="errors"> {{Session::get('emmail1')}}</p>
					@endif
					<input class="text email" type="email" name="email1" placeholder="Email" value="{{old('email')}}">
					<input type="submit" value="Restore">
			    @endif
				</form>
				@if(Session::has('ka'))
				<form action="{{URL::to('/confirm')}}" method="post">
					{{csrf_field()}}
					<p class="errors"> {{Session::get('res')}}</p>
					@if($errors->has('code'))
					    <p class="errors">{{$errors->first('code')}}</p>
					@endif
					@if(Session::has('sxalcode'))
					    <p class="errors">{{Session::get('sxalcode')}}</p>
					@endif
					<input class="text email" type="text" name="code" placeholder="Code" >
					@if($errors->has('password'))
					    <p class="errors">{{$errors->first('password')}}</p>
					@endif
					<input class="text email" type="password" name="password" placeholder="Password" >
					@if($errors->has('conpassword'))
					    <p class="errors">{{$errors->first('conpassword')}}</p>
					@endif
					<input class="text email" type="password" name="conpassword" placeholder="Confirm Password" >
					<input type="submit" value="Confirm">
				</form>
				@endif
				<p>Don't have an Account? <a href="{{URL::to('/signup')}}">Register</a></p>
				<p>Don't have an Account? <a href="{{URL::to('/login')}}">  Login Now! </a></p>
			</div>
		</div>
		<!-- copyright -->
		<!-- <div class="colorlibcopy-agile">
			<p>© 2018 Colorlib Signup Form. All rights reserved | Design by <a href="https://colorlib.com/" target="_blank">Colorlib</a></p>
		</div> -->
		<!-- //copyright -->
		<ul class="colorlib-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
	<!-- //main -->
</body>
</html>