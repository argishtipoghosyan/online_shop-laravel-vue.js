@extends('layouts.profile_layout')




@section('container')
			<section class="main-content">
				<div class="row">
					<div class="span12">													
						<div class="row">
							<div class="span12">
								<h4 class="title">
									<span class="pull-left"><span class="text"><span class="line">My <strong>Products</strong></span></span></span>
									<span class="pull-right">
										<a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button" href="#myCarousel" data-slide="next"></a>
									</span>
								</h4>
								<div id="myCarousel" class="myCarousel carousel slide">
									<div class="carousel-inner">
										<div class="active item">
											<ul class="thumbnails">		
											@if($products!=[])
											  @foreach($products as $product)					
												<li class="span3">
													@if($product->status!='0' && $product->status!='1')
													    <div class="product-box" style="background-color: red !important">
													@elseif($product->status=='0') 
													    <div class="product-box" style="background-color: #e79c2d !important">	
													@else
													    <div class="product-box">
													@endif
														<span class="sale_tag"></span>
														@if($product->product_image->count()!=0)
														<p><a href="{{URL::to('product_detail'.'/'.$product['id'])}}"><img src="{{URL::asset($product->product_image[0]['name'])}}" alt="" /></a></p>
														@endif
														<a href="{{URL::to('product_detail'.'/'.$product['id'])}}" class="title">{{$product['name']}}</a><br/>
														<a href="{{URL::to('product_detail'.'/'.$product['id'])}}" class="category">{{$product['description']}}</a>
														<p class="price">${{$product['price']}}</p>
														@if($product->status=='0')
     														<p style="color: #fff">Not approved by Admin</p>
													    @endif
													    @if($product->status!='0' && $product->status!='1')
     														<p style="color: #fff">Rejected</p>
													    @endif 
														@if($product['star']=='0')
															<i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
														@else
                                                            <i class="fa fa-star-o {{$product['star']>=1?'starcolor':''}}"></i>
                                                            <i class="fa fa-star-o {{$product['star']>=2?'starcolor':''}}"></i>
                                                            <i class="fa fa-star-o {{$product['star']>=3?'starcolor':''}}"></i>
                                                            <i class="fa fa-star-o {{$product['star']>=4?'starcolor':''}}"></i>
                                                            <i class="fa fa-star-o {{$product['star']>=5?'starcolor':''}}"></i>
														@endif
													</div>
													
												</li>
											  @endforeach
											@endif
											</ul>
										</div>		
									</div>							
								</div>
							</div>						
						</div>					
					</div>				
				</div>
			</section>
			
			
	@endsection('container')
