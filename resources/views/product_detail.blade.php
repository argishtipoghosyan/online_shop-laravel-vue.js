@extends('layouts.profile_layout')




@section('container')
			<section class="header_text sub">
				<h4><span>Product Detail</span></h4>
			</section>
			<section class="main-content">				
						<div class="row">
							<div class="span4">
								@if(count($product->product_image)!=0)
								<a href="" class="thumbnail" data-fancybox-group="group1" title="Description 1"><img alt="" src="{{URL::asset($product->product_image[0]['name'])}}"></a>												
								@endif
								<ul class="thumbnails small">
		                        @foreach($product->product_image as $image)
                     			    <li class="span1" id="{{$image['id']}}">
										<a href="" class="thumbnail" data-fancybox-group="group1" title="Description 2">
											<img src="{{URL::asset($image['name'])}}" alt=""></a>
							                @if($product['user_id']==$user_id )
                                                <i data-id='{{$image["id"]}}' class="fa fa-times-circle nkarjnjel" ></i>
											@endif
									</li>	
								@endforeach		
								</ul>
							</div>
							<div class="span5">
								<address>
									<strong>Name:</strong> <span>{{$product['name']}}</span><br>
									<strong>Count:</strong> <span>{{$product['count']}}</span><br>
									<strong>Description:</strong> <span>{{$product['description']}}</span><br>
								</address>									
								<h4><strong>Price: ${{$product['price']}}</strong></h4>
							</div>
							@if($product['user_id']!=$user_id)
							<div class="span5" data-id="">
								<form class="form-inline">
									<label class="checkbox">
										<input type="checkbox" value=""> Option one is this and that
									</label>
									<br/>
									<label class="checkbox">
									  <input type="checkbox" value=""> Be sure to include why it's great
									</label>
									<p>&nbsp;</p>
									<label>Qty:</label>
									<input type="text" class="span1" placeholder="1">
									<button class="btn btn-inverse" type="submit">Add to cart</button>
								</form>
							</div>
							@endif
							<div class="row1" style="float: right;margin-top: -35px;">
							    @if($product['user_id']==$user_id)
			 			           <form  class="updatproduct" enctype="multipart/form-data" >
						           {{csrf_field()}}

						            <div class="control-group">
						            	<label class="control-label">Name:</label>
						            	@if($errors->has('name'))
                                            <p class="errors"> {{$errors->first('name')}}</p>
					                    @endif
		    	     	            	<div class="controls">
						            		<input type="text" placeholder="Name" value="{{$product['name']}}" name="name" class="input-xlarge">
						            	</div>
						            </div>	
                                    <div class="control-group">
						            	<label class="control-label">Count:</label>
						            	@if($errors->has('count'))
                                            <p class="errors"> {{$errors->first('count')}}</p>
                                        @endif
		    	     	            	<div class="controls">
						            		<input type="text" placeholder="Count" value="{{$product['count']}}" name="count" class="input-xlarge">
						            	</div>
						            </div>	
						            <div class="control-group">
						            	<label class="control-label">Price:</label>
						            	@if($errors->has('price'))
                                            <p class="errors"> {{$errors->first('price')}}</p>
                                        @endif
		    	     	            	<div class="controls">
						            		<input type="text" placeholder="Price" value="{{$product['price']}}" name="price" class="input-xlarge">
						            	</div>
						            </div>	
						            <div class="control-group">
						            	<label class="control-label">Description:</label>
						            	@if($errors->has('description'))
                                            <p class="errors"> {{$errors->first('description')}}</p>
                                        @endif
                                        <div class="controls">
						        		     <input type="text" placeholder="Description" value="{{$product['description']}}" name="description" class="input-xlarge">
							        </div>
							        <div class="control-group">
							            <label class="control-label">Image:</label>
							            @if($errors->has('image'))
                                            <p class="errors"> {{$errors->first('image')}}</p>
                                        @endif
		    	     		            <div class="controls">
						     		        <input type="file" placeholder="Image" name="image[]" multiple class="input-xlarge">
							            </div>
			             			</div>
							        <input type="hidden" name="id0" value="{{$product['id']}}">
							    	<button class="btn btn-inverse update"  data-id='{{$product["id"]}}' type="button">Update</button>
						           </form>
						            <button class="btn btn-inverse" data-id='{{$product["id"]}}' type="submit"><a href="{{URL::to('/delete').'/'.$product['id']}}">Delete</a></button>
								@endif
						    </div>	
								
					
						</div>							
						
			</section>					

	@endsection('container')
 