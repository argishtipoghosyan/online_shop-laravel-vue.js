@extends('layouts.profile_layout')




@section('container')

			
			   <div>					
					<h4 class="title"><span class="text"><strong>Add</strong> Product</span></h4>
					<form action="{{URL::to('/addprod_form')}}" enctype="multipart/form-data" method="post" class="form-stacked dropzone">
						{{csrf_field()}}
						
						<div class="control-group">
							<label class="control-label">Name:</label>
							@if($errors->has('name'))
                                <p class="errors"> {{$errors->first('name')}}</p>
					        @endif
		    	     		<div class="controls">
								<input type="text" placeholder="Name" name="name" class="input-xlarge">
							</div>
						</div>	
                        <div class="control-group">
							<label class="control-label">Count:</label>
							@if($errors->has('count'))
                                <p class="errors"> {{$errors->first('count')}}</p>
					        @endif
		    	     		<div class="controls">
								<input type="text" placeholder="Count" name="count" class="input-xlarge">
							</div>
						</div>	
						<div class="control-group">
							<label class="control-label">Price:</label>
							@if($errors->has('price'))
                                <p class="errors"> {{$errors->first('price')}}</p>
					        @endif
		    	     		<div class="controls">
								<input type="text" placeholder="Price" name="price" class="input-xlarge">
							</div>
						</div>	
						<div class="control-group">
							<label class="control-label">Description:</label>
							@if($errors->has('description'))
                                <p class="errors"> {{$errors->first('description')}}</p>
					        @endif
		    	     		<div class="controls">
								<input type="text" placeholder="Description" name="description" class="input-xlarge">
							</div>
						</div>	
						<div class="control-group">
							<label class="control-label">Image:</label>
							@if($errors->has('image'))
                                <p class="errors"> {{$errors->first('image')}}</p>
					        @endif
		    	     		<div class="controls">
								<input type="file" placeholder="Image" name="image[]" multiple class="input-xlarge">
							</div>
						</div>
						<div class="actions"><input tabindex="9" class="btn btn-inverse large" type="submit" value="Add Product"></div>		
					</form>					
				</div>        
			
	@endsection('container')
			