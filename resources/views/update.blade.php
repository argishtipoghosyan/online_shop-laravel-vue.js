@extends('layouts.profile_layout')

@section('container')


					
			<section class="header_text sub">
			
				<h4><span>Update</span></h4>
			</section>			
			<section class="main-content">				
				<div class="row">
					<div class="span5">					
						<h4 class="title"><span class="text"><strong>Password</strong> Form</span></h4>
						
                            <form action="{{URL::to('/uppas_form')}}" method="post">
							{{csrf_field()}}

							<input type="hidden" name="next" value="/">
							<fieldset>
								<div class="control-group">
									<label class="control-label">Password</label>
									@if($errors->has('password'))
                                        <p class="errors"> {{$errors->first('password')}}</p>
				                	@endif
									@if(Session::has('pass_error'))
                                        <p class="errors"> {{Session::get('pass_error')}}</p>
				                	@endif
									<div class="controls">
										<input type="password" placeholder="Enter your password"  class="input-xlarge" name="password">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">New password</label>
									@if($errors->has('newpassword'))
                                        <p class="errors"> {{$errors->first('newpassword')}}</p>
				                	@endif
									<div class="controls">
										<input type="password" placeholder="Enter your New password" class="input-xlarge" name="newpassword">
									</div>
								</div>
                                <div class="control-group">
									<label class="control-label">New Repead password</label>
									@if($errors->has('reppassword'))
                                        <p class="errors"> {{$errors->first('reppassword')}}</p>
					                @endif
									<div class="controls">
										<input type="password" placeholder=" New Repead password" class="input-xlarge" name="reppassword">
									</div>
								</div>

								<div class="control-group">
									<input tabindex="3" class="btn btn-inverse large" type="submit" value="Update Password">
								</div>
							</fieldset>
						    </form>
									
					</div>
					<div class="span7">					
						<h4 class="title"><span class="text"><strong>Update</strong> Form</span></h4>
						<form action="{{URL::to('/up_form')}}" method="post" class="form-stacked">
							{{csrf_field()}}
							<div class="control-group">
									<label class="control-label">Username</label>
									@if($errors->has('username'))
                                        <p class="errors"> {{$errors->first('username')}}</p>
                                    @endif
									<div class="controls">
										<input type="text" value="{{$user->name}}" name="username" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Surname</label>
									@if($errors->has('surname'))
                                        <p class="errors"> {{$errors->first('surname')}}</p>
                                    @endif
									<div class="controls">
										<input type="text" value="{{$user->surname}}" name="surname" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Age</label>
									@if($errors->has('age'))
                                        <p class="errors"> {{$errors->first('age')}}</p>
                                    @endif
									<div class="controls">
										<input type="text" value="{{$user->age}}" name="age" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Email address:</label>
									@if($errors->has('email'))
                                        <p class="errors"> {{$errors->first('email')}}</p>
                                    @endif
									<div class="controls">
										<input type="text" value="{{$user->email}}" name="email" class="input-xlarge">
									</div>
								</div>
								
								<div class="actions"><input tabindex="9" class="btn btn-inverse large" type="submit" value="Update your account"></div>
							</fieldset>
						</form>					
					</div>				
				</div>
			</section>			
	@endsection('container')
			



