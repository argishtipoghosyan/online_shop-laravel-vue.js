@extends('layouts.profile_layout')




@section('container')

<section class="main-content">				
				<div class="row">
					<div class="span9">					
						<h4 class="title"><span class="text"><strong>Your</strong> Cart</span></h4>
								@if($zambyux->count()!=0)
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Image</th>
									<th>Star</th>
									<th>Product Name</th>
									<th>Quantity</th>
									<th>edit</th>
									<th>Unit Price</th>
									<th>Total</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
										  		  
								@foreach($apranq as $p)
								<tr class="product1" >
									<td><a href="{{URL::to('product_detail'.'/'.$p['id'])}}"><img alt="" style="width: 33%" src="{{URL::asset($p->product_image[0]['name'])}}"></a></td>
									<td>
										@if($p['star']==0)
                                        <i class="fa fa-star-o "></i>
                                        <i class="fa fa-star-o "></i>
                                        <i class="fa fa-star-o "></i>
                                        <i class="fa fa-star-o "></i>
                                        <i class="fa fa-star-o "></i>                                        
                                        @else

                                        <i class="fa fa-star-o {{$p['star']>=1?'starcolor':''}}"></i>
                                        <i class="fa fa-star-o {{$p['star']>=2?'starcolor':''}}"></i>
                                        <i class="fa fa-star-o {{$p['star']>=3?'starcolor':''}}"></i>
                                        <i class="fa fa-star-o {{$p['star']>=4?'starcolor':''}}"></i>
                                        <i class="fa fa-star-o {{$p['star']>=5?'starcolor':''}}"></i>     
										@endif
									</td>
									<td>{{$p['name']}}</td>
                                <form action="{{URL::to('/tarmacnel')}}" method="post">
					                {{csrf_field()}}
									<td><input type="text" placeholder="{{$p->p_zambyux[0]['qanak']}}" value="{{$p->p_zambyux[0]['qanak']}}" class="input-mini" name="tarmacnel"></td>
									<input type="hidden" name="id" value="{{$p['id']}}">
									<td><button class="btn btn-inverse avelacnel" type="submit">edit</button></td>
                                </form>
									<td>${{$p['price']}}</td>
									<td>${{$p['price']*$p->p_zambyux[0]['qanak']}}</td>
								<form action="{{URL::to('/jnjel')}}" method="post">
					                {{csrf_field()}}
									<td><button style="border: none; background-color: #fff;"><i  class="fa fa-trash heracnel"></i></button></td>
									<input type="hidden" name="i" value="{{$p['id']}}">
								</form>
								</tr>
								@endforeach
							</tbody>
							<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
						</table>		
				</div>
					<div class="span3 col">
						<div class="block">	
							<ul class="nav nav-list">
								<li class="nav-header"><h2>Order price</h2></li>	
								<li><h5>Subtotal US ${{$x}}</h5></li>
								
									<input type="hidden" name="gin" value="{{$x}}">

								<li><h2>Total US ${{$x}}</h2></li>
								<li><button  class="btn_buy" ><a href="{{URL::to('/stripe')}}">BUY</a></button></li>
							    
							</ul>
						</div>					
					</div>
				</div>
								@endif			  		  
			</section>

@endsection('container')