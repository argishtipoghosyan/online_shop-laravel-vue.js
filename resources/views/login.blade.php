<!--
Author: Colorlib
Author URL: https://colorlib.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Creative Colorlib SignUp Form</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="{{URL::asset('css/signup.css')}}" rel="stylesheet" type="text/css" media="all" />
<!-- //Custom Theme files -->
<!-- web font -->
<link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
<!-- //web font -->
</head>
<body>
	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>Log In</h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				<form action="{{URL::to('/login_form')}}" method="post">
					{{csrf_field()}}
					@if($errors->has('email'))
                       <p class="errors"> {{$errors->first('email')}}</p>
					@endif
					@if(Session::has('error_email'))
                       <p class="errors"> {{Session::get('error_email')}}</p>
					@endif
					<input class="text email" type="email" name="email" placeholder="Email" value="{{old('email')}}">
					@if($errors->has('password'))
                       <p class="errors"> {{$errors->first('password')}}</p>
					@endif
					@if(Session::has('pass_error'))
                       <p class="errors"> {{Session::get('pass_error')}}</p>
					@endif
					<input class="text" type="password" name="password" placeholder="Password" >
					<div class="wthree-text">
						<label class="anim">
				            <p>Don't have an Account? <a href="{{URL::to('/signup')}}">Register</a></p>
							
						</label>
						<div class="clear"> </div>
					</div>
					<input type="submit" value="Log In">
				</form>
			        <p>Forgot your account? <a href="{{URL::to('/Forgot')}}"> Restore </a></p>
				
			</div>
		</div>
		<!-- copyright -->
		<!-- <div class="colorlibcopy-agile">
			<p>© 2018 Colorlib Signup Form. All rights reserved | Design by <a href="https://colorlib.com/" target="_blank">Colorlib</a></p>
		</div> -->
		<!-- //copyright -->
		<ul class="colorlib-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
	<!-- //main -->
</body>
</html>