<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Bootstrap E-commerce Templates</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">      
		<link href="{{URL::asset('css/bootstrap.css')}}" rel="stylesheet">      
		<link href="{{URL::asset('css/bootstrap-responsive.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('css/bootstrap-responsive.css')}}" rel="stylesheet">
		
		<link href="{{URL::asset('css/bootstrappage.css')}}" rel="stylesheet"/>
		<link href="{{URL::asset('css/jquery.fancybox.css')}}" rel="stylesheet"/>
		
		<link href="{{URL::asset('css/flexslider.css')}}" rel="stylesheet"/>
		<link href="{{URL::asset('css/main.css')}}" rel="stylesheet"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



		
		<script src="{{URL::asset('js/jquery-1.7.2.min.js')}}"></script>
		<script src="{{URL::asset('js/jquery.fancybox.js')}}"></script>	
		<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>				
		<script src="{{URL::asset('js/superfish.js')}}"></script>	
		<script src="{{URL::asset('js/jquery.scrolltotop.js')}}"></script>
		<script src="{{URL::asset('js/ajax.js')}}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


        <meta name="csrf-token" content="{{ csrf_token() }}" />
 
    
		
		<!--[if lt IE 9]>			
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
    <body>		
		<div id="top-bar" class="container">
			<div class="row">
				<div class="span4">
					<form method="POST" class="search_form">
						<input type="text" class="input-block-level search-query" Placeholder="eg. T-sirt">
					</form>
				</div>
				<div class="span8">
					<div class="account pull-right">
						<ul class="user-menu">				
							<li><a href="{{URL::to('/profile')}}">My Account</a></li>
							<li><a href="{{URL::to('/update')}}">Update</a></li>
							<li><a href="{{URL::to('/product')}}">Add Product</a></li>
     					    <li><a href="{{URL::to('/myproduct')}}">My Product</a></li>
     					    <li><a href="{{URL::to('/cart')}}">Cart</a></li>	
     					    <li><a href="{{URL::to('/patver')}}">Purchased</a></li>	
     					    <li><a href="{{URL::to('/logout')}}">Logout</a></li>	
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="wrapper" class="container">
			<!-- popoxutyun -->
			<section class="navbar main-menu">
				<div class="navbar-inner main-menu">				
					<a href="{{URL::to('/cart')}}" class="logo pull-left"><img src="{{URL::asset('themes/images/logo.png')}}" class="site_logo" alt=""></a>
					
				</div>
			</section>
			
			<section  class="homepage-slider" id="home-slider">
     			<img class="pageBanner" src="{{URL::asset('themes/images/pageBanner.png')}}" alt="New products" >
				<div class="flexslider">
					<ul class="slides">
						<li>
							<img src="{{URL::asset('themes/images/carousel/banner-1.jpg')}}" alt="" />
						</li>
						<li>
							<img src="{{URL::asset('themes/images/carousel/banner-2.jpg')}}" alt="" />
							<div class="intro">
								<h1>Mid season sale</h1>
								<p><span>Up to 50% Off</span></p>
								<p><span>On selected items online and in stores</span></p>
							</div>
						</li>
					</ul>
				</div>			
			</section>

     @yield('container')

			<section id="footer-bar">
				<div class="row">
					
					<div class="span4">
						<h4>My Account</h4>
						<ul class="nav">
							<li><a href="{{URL::to('/profile')}}">My Account</a></li>
							<li><a href="{{URL::to('/update')}}">Update</a></li>
							<li><a href="{{URL::to('/product')}}">Add Product</a></li>
							<li><a href="{{URL::to('/myproduct')}}">My Product</a></li>
     					    <li><a href="{{URL::to('/cart')}}">Cart</a></li>
     					    <li><a href="{{URL::to('/patver')}}">Purchased</a></li>	

     					    <li><a href="{{URL::to('/logout')}}">Logout</a></li>
						</ul>
					</div>
					<div class="span5">
						<a href="{{URL::to('/cart')}}" class="logo pull-left">
						<p class="logo"><img src="{{URL::asset('themes/images/logo.png')}}" class="site_logo" alt=""></p>
					    </a>
						
						<span class="social_icons">
							<a class="facebook" href="#">Facebook</a>
							<a class="twitter" href="#">Twitter</a>
							<a class="skype" href="#">Skype</a>
							<a class="vimeo" href="#">Vimeo</a>
						</span>
					</div>					
				</div>	
			</section>
			<section id="copyright">
				<span>Copyright 2020 bootstrappage template  All right reserved.</span>
			</section>
		</div>
		<script src="{{URL::asset('js/common.js')}}"></script>
		<script type="text/javascript">
			$(function() {
				$(document).ready(function() {
					$('.flexslider').flexslider({
						animation: "fade",
						slideshowSpeed: 4000,
						animationSpeed: 600,
						controlNav: false,
						directionNav: true,
						controlsContainer: ".flex-container" // the container that holds the flexslider
					});
				});
			});
		</script>
		<script>
			$(function () {
				$('#myTab a:first').tab('show');
				$('#myTab a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
			})
			$(document).ready(function() {
				$('.thumbnail').fancybox({
					openEffect  : 'none',
					closeEffect : 'none'
				});
				
				$('#myCarousel-2').carousel({
                    interval: 2500
                });								
		?	});
		</script>
    </body>
</html>