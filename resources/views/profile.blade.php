@extends('layouts.profile_layout')




@section('container')
						
						<section class="header_text">
				<a href="{{URL::to('/profile')}}">{{$user->name}} {{$user->surname}}</a>
				

			</section>
			<section class="main-content">
				<div class="row">
					<div class="span12">													
						<div class="row">
							<div class="span12">
								<h4 class="title">
									<span class="pull-left"><span class="text"><span class="line">Feature <strong>Products</strong></span></span></span>
									<span class="pull-right">
										<a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button" href="#myCarousel" data-slide="next"></a>
									</span>
								</h4>
								<div id="myCarousel" class="myCarousel carousel slide">
									<div class="carousel-inner">
										<div class="active item">
											<ul class="thumbnails">		
											@foreach($products as $product)
											@if($product['count']!=0)									
																	
												<li class="span3">
													<div class="product-box">
														<span class="sale_tag"></span>
														@if($product->product_image->count()!=0)
														<p><a href="{{URL::to('product_detail'.'/'.$product['id'])}}"><img src="{{URL::asset($product->product_image[0]['name'])}}" alt="" /></a></p>
														@endif
														<a href="{{URL::to('product_detail'.'/'.$product['id'])}}" class="title">{{$product['name']}}</a><br/>
														<a href="{{URL::to('product_detail'.'/'.$product['id'])}}" class="category">{{$product['description']}}</a><br>
														<a href="{{URL::to('product_detail'.'/'.$product['id'])}}" class="category">{{$product['count']}}</a>
														<p class="price">${{$product['price']}}</p>
														<p>
															@if($product['star']==0)
															<i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
                                                            <i class="fa fa-star-o"></i>
															@else
                                                            <i class="fa fa-star-o {{$product['star']>=1?'starcolor':''}}"></i>
                                                            <i class="fa fa-star-o {{$product['star']>=2?'starcolor':''}}"></i>
                                                            <i class="fa fa-star-o {{$product['star']>=3?'starcolor':''}}"></i>
                                                            <i class="fa fa-star-o {{$product['star']>=4?'starcolor':''}}"></i>
                                                            <i class="fa fa-star-o {{$product['star']>=5?'starcolor':''}}"></i>
															@endif
														</p>
                                                        @if($user->id!=$product['user_id'])
									                    <input type="text" class="span1 zambyux" placeholder="1" value="1" name="qanak">
									                    <button class="btn btn-inverse gnel" type="submit"  data-id="{{$product['id']}}">gnel</button>
                                                        @endif
													</div>
												</li>
												
											@endif	
											@endforeach
											
											</ul>
										</div>
									
									</div>							
								</div>
							</div>						
						</div>
						<br/>
					
								
					</div>				
				</div>
			</section>
			
			
	@endsection('container')
