




@extends('layouts.profile_layout')




@section('container')

<section class="main-content">				
				<div class="row">
					<div class="span12">					
						<h4 class="title"><span class="text"><strong>Your</strong> Purchases</span></h4>
						@if($zambyux->count()!=0)
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Image</th>
									<th>Star</th>
									<th>Product Name</th>
									<th>Quantity</th>
									<th>Unit Price</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>	  		  
								@foreach($apranq as $p)
								    <tr class="product1" >
								    	<td><a href="{{URL::to('product_detail'.'/'.$p['id'])}}"><img alt="" style="width: 12%" src="    {{URL::asset($p->product_image[0]['name'])}}"></a></td>
								    	<td data-id="{{$p['id']}}">
                                            @if($p->p_apranq[0]['star']==0)
								    		    <i class="fa fa-star-o star"></i><i class="fa fa-star-o star"></i><i class="fa fa-star-o star"></i><i class="fa fa-star-o star"></i><i class="fa fa-star-o star"></i>
                                            @else 
                                                <i class="fa fa-star-o {{$p->p_apranq[0]['star']>=1?'starcolor':''}}"></i>
                                                <i class="fa fa-star-o {{$p->p_apranq[0]['star']>=2?'starcolor':''}}"></i>
                                                <i class="fa fa-star-o {{$p->p_apranq[0]['star']>=3?'starcolor':''}}"></i>
                                                <i class="fa fa-star-o {{$p->p_apranq[0]['star']>=4?'starcolor':''}}"></i>
                                                <i class="fa fa-star-o {{$p->p_apranq[0]['star']>=5?'starcolor':''}}"></i>     
                                            @endif
								    	</td>
								    	<td>{{$p['name']}}</td>
								    	<td>{{$p->p_apranq[0]['qanak']}}</td>
								    	<td>${{$p['price']}}</td>
								    	<td>${{$p['price']*$p->p_apranq[0]['qanak']}}</td>
								    </tr>
								@endforeach
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									
								</tr>
							</tbody>
						</table>
						@endif			  		  

				    </div>
				</div>
</section>

@endsection('container')