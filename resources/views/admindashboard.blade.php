<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	    <title></title>      

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		 <meta name="description" content="">
		<link href="{{URL::asset('css/bootstrap.css')}}" rel="stylesheet">		<link href="{{URL::asset('css/bootstrap-responsive.min.css')}}" rel="stylesheet">
		<link href="{{URL::asset('css/bootstrappage.css')}}" rel="stylesheet"/>
		<link href="{{URL::asset('css/jquery.fancybox.css')}}" rel="stylesheet"/>
		<link href="{{URL::asset('css/flexslider.css')}}" rel="stylesheet"/>
		<link href="{{URL::asset('css/main.css')}}" rel="stylesheet"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div id='app' >
        <router-view :products="{{$products}}" :users="{{$users}}"></router-view>
    </div>
</body>
<script src="{{URL::asset('js/app.js')}}"></script>
</html> 