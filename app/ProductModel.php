<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
   public $table = 'product';

    function product_image(){
      	return $this->hasMany('App\ProductimageModel','product_id');
    }
    
    function p_zambyux(){
      	return $this->hasMany('App\ZambyuxModel','product_id');
    }
    function p_apranq(){
        return $this->hasMany('App\PatverModel','product_id');
    }


     function product_user(){

   	return $this->belongsTo('App\UsersModel','user_id');

   }
}
