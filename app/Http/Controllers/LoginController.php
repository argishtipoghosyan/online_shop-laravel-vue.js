<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Auth;
class LoginController extends Controller
{
    function  login(){
    	return view('login');
    }
    function login_form(request $data){
        $validatedUser = $data->validate([
            'email' => 'required',
            'password' => 'required',
        ]); 
    	$email = $data->email;
        $user = UsersModel::where('email',$email)->first();
         
        
        if(!empty($user)){

            if (Hash::check($data->password,$user->password)) {
                if($user->active!=0){
                    if($user->time<time()){
                        if($user->status=='user'){
                            Session::put('user_id',$user->id);
                            return redirect('/profile');
                        }
                        else{
                            Session::put('admin_id',$user->id);
                            return redirect('/admindashboard');
                        }
                    }
                    else{
                        Session::flash('error_email','Your profile is blocked');
                        return back();
                    }
                }
                else{
                    Session::flash('error_email','An email has been sent to this email, please reply');
                return back();
                }
            }
            else{
                Session::flash('pass_error','The password is incorrect');
                Session::flash('email_user',$user->email);
                return back();
            }
        }
        else{
            Session::flash('error_email','No registration');
            Session::flash('email_user',$data->email);
            return back();
        }

    }
}
