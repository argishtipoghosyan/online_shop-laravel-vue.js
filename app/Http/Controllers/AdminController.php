<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductModel;
use App\UsersModel;

use App\PatverModel;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    function admin(){
        $products = ProductModel::all()->where('status','0');
        $users = UsersModel::all()->where('active','1')->where('status','user');
        if($products->count()!=0){
            foreach($products as $a){
                $k = PatverModel::where('product_id',$a->id);
                if($k->count()!=0){
                    $j = $k->first();
                    $a['star'] = $k->avg('star');
                    }
                else{
                    $j = $k->first();
                    $a['star'] = 0;
                }
 
                $a['product_image'] = $a->product_image;    
            }
        }
        
        return view('admindashboard',compact('products','users'));
    }

    function logoutAdmin(){
        Session::flush();
        return redirect('/login');

    }
    function block(Request $data){
        UsersModel::where('id', $data->id)->update([
            'time' => $data->jam
        ]);
    }
    function itempost(Request $data){
        ProductModel::where('id', $data->id)->update([
            'status' => $data->title
        ]);
        
    }
    function allowpost(Request $data){
        ProductModel::where('id', $data->prod_id)->update([
            'status' => 1
        ]);
       
    }
}
