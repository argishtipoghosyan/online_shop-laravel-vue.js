<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\UsersModel;
use App\ProductModel;
use App\PatverModel;


class UserController extends Controller
{
	function profile(){
        $id = Session::get('user_id');
        $user = UsersModel::where('id',$id)->first();
        $products = ProductModel::all()->where('status','1');
        if($products->count()!=0){
            foreach($products as $a){
                $k = PatverModel::where('product_id',$a->id);
                if($k->count()!=0){
                    $j = $k->first();
                    $a['star'] = $k->avg('star');
                    }
                else{
                    $j = $k->first();
                    $a['star'] = 0;
                }    
            }
        }
        return view('profile',compact('user','products'));
    }
       
    
    function logout(){
        Session::flush();
        return redirect('/login');
    }

    function update(){
        $id = Session::get('user_id');
        $user = UsersModel::where('id',$id)->first();
        return view('update',compact('user'));
    }
    function uppas_form(Request $data){
        $id = Session::get('user_id');
        $user = UsersModel::where('id',$id)->first();
            $validatedUser = $data->validate([
                'password' => 'required',
                'newpassword' => 'required',
                'reppassword' => 'required',
            ]); 
        if($data->newpassword == $data->reppassword){
            if(Hash::check($data->password,$user->password)){
                UsersModel::where('id', $id)->update([
                    'password' => Hash::make($data->newpassword)
                ]);
                return redirect('/profile');
            }
        }
        else{
            Session::flash('pass_error','The password is incorrect');
            return back();
        }
        
    }
    
    function up_form(Request $data){
        $id = Session::get('user_id');
        $user = UsersModel::where('id',$id)->first();
        if($user->email!=$data->email){
            $validatedUser = $data->validate([
                'username' => 'required',
                'surname' => 'required',
                'age' => 'required|numeric',
                'email' => 'required|email:rfc,dns|unique:users,email',
            ]); 
        }
        else{
            $validatedUser = $data->validate([
                'username' => 'required',
                'surname' => 'required',
                'age' => 'required|numeric',
            ]);
        }
        UsersModel::where('id', $id)->update([
                'name' => $data->username,
                'surname' => $data->surname,
                'age' => $data->age,
                'email' => $data->email
            ]);
        return redirect('/profile');
    }
    
}    