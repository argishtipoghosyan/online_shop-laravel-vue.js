<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\ProductModel;
use App\UsersModel;
use App\ZambyuxModel;
use App\PatverModel;
use App\ProductimageModel;

class ProductController extends Controller
{
    function product_detail($id){
        $user_id = Session::get('user_id');
            $product = ProductModel::where('id',$id)->first();
            return view('product_detail',compact('product','user_id'));
            
         
    }
    function product(){
        
    	return view('product');
        
    }
    function delete($data){
        ProductModel::where('id',$data)->delete();
        return redirect('/profile');
    }
    function tarmacnel(Request $data){
        $user_id = Session::get('user_id');
        $d = ProductModel::where('id', $data->id)->first();
        $count = ZambyuxModel::where('user_id',$user_id)->where('product_id',$data->id)->first();
        if($data->tarmacnel !=''){
            if($d->count>=$data->tarmacnel){
                ZambyuxModel::where('user_id',$user_id)->where('product_id',$data->id)->update([
                    'qanak' => $data->tarmacnel
                ]);            
            }
        }
            return back();
    }
    function zambyux(Request $data){  
        $user_id = Session::get('user_id');
        $d = ProductModel::where('id', $data->id)->first();
        $count = ZambyuxModel::where('user_id',$user_id)->where('product_id',$data->id)->first();
        $valid = $data->validate([
            'qanak' => 'required'
        ]);
        
        if($count!=[]){
            if($d->count>=$data->qanak+$count->qanak){
                ZambyuxModel::where('user_id',$user_id)->where('product_id',$data->id)->update([
                    'qanak' => $data->qanak+$count->qanak,
                ]);            
                return[$d->count,$data->qanak+$count->qanak];
            }
            Session::put('zambyux',$count->qanak);
            
        }
        else{
            if($d->count>=$data->qanak){
                $apranq = new ZambyuxModel;
                $apranq->product_id = $data->id;
                $apranq->qanak = $data->qanak;
                $apranq->user_id = $user_id;
                $apranq->save();
                return[$d->count,$data->qanak];
           }
        } 
    }
    function apranq(){  
        $id = Session::get('user_id');
        $zambyux = PatverModel::all()->where('user_id',$id);
        $x=0;
        if($zambyux->count()!=0){
            // dd($zambyux->count());
        foreach($zambyux as $a){
            $apranq[] = ProductModel::where('id',$a->product_id)->first();
            $x += $a->qanak*ProductModel::where('id',$a->product_id)->first()->price;    
        }    

        return view('patver',compact('apranq','x','zambyux'));
        }
        else{
            return view('patver',compact('zambyux'));
        }
    }
    function delimg(Request $data){
        $a = ProductimageModel::where('id',$data->id )->first();
        echo $a->name;
        unlink(substr($a->name,1));
        ProductimageModel::where('id',$data->id )->delete();   
    }

    function star(Request $data){
        $user_id = Session::get('user_id');
        PatverModel::where('product_id',$data->id)->where('user_id',$user_id)->update([
            'star' => $data->star
        ]);
    }

    function update(Request $data){
        $validatedUser = $data->validate([
            'name' => 'required',
            'count' => 'required|numeric',
            'price' => 'required|numeric', 
        ]);
        $id = $data->id0;
        ProductModel::where('id', $id)->update([
            'name' => $data->name,
            'count' => $data->count,
            'price' => $data->price,
            'description' => $data->description,
            'status' => 0
        ]);
        $image=[];
        if($data->hasfile('image')){
            foreach($data->file('image') as $file){
                $name=time().$file->getClientOriginalName();
                $file->move(public_path().'/productimage/', $name);  
                $productimage = new ProductimageModel;
                $productimage->name ='/productimage/'. $name;
                $productimage->product_id = $id  ;
                $productimage->save();
                $image[] = ['name'=>"http://online_shop.am/productimage/$name",'id'=>$productimage->id];
            }
        }
        $product = ProductModel::where('id',$id)->first();
        return [$product,$image];
    }
    function cart(){
        $id = Session::get('user_id');
        $zambyux = ZambyuxModel::all()->where('user_id',$id);
        $x=0;
        if($zambyux->count()!=0){
            foreach($zambyux as $a){
                $h = ProductModel::where('id',$a->product_id)->first();
                if(PatverModel::where('product_id',$a->product_id)->count()!=0){
                    $j = PatverModel::where('product_id',$a->product_id)->first();
                    $h['star'] = PatverModel::where('product_id',$a->product_id)->avg('star');
                }
                else{
                    $j = PatverModel::where('product_id',$a->product_id)->first();
                    $h['star'] = 0;
                }
                $apranq[] = $h;
                $x += $a->qanak*ProductModel::where('id',$a->product_id)->first()->price;    
                }   
                Session::put('gin',$x);
                return view('cart',compact('apranq','x','zambyux'));
            }
        else{
            return view('cart',compact('zambyux'));
        }
    }
    function myproduct(){
        $id = Session::get('user_id');
        $products = ProductModel::all()->where('user_id',$id);
        if($products->count()!=0){
            foreach($products as $a){
                $k = PatverModel::where('product_id',$a->id);
                if($k->count()!=0){
                    $j = $k->first();
                    $a['star'] = $k->avg('star');
                }
                else{
                    $j = $k->first();
                    $a['star'] = 0;
                }        
            }
        }
        return view('myproduct',compact('products')); 
    }
    function jnjel(Request $data){
        $id = Session::get('user_id');
        ZambyuxModel::where('product_id',$data->i)->where('user_id',$id)->delete(); 
        return redirect('/cart');               
    }
    function addprod_form(Request $data){
        $id = Session::get('user_id');
    	$validatedUser = $data->validate([
            'image' => 'required',
            'name' => 'required',
            'count' => 'required|numeric',
            'price' => 'required|numeric',
            'description' => 'required'
        ]); 
        $product = new ProductModel;
        $product->name = $data->name;
        $product->count = $data->count;
        $product->price = $data->price;
        $product->description = $data->description;
        $product->user_id = $id;
        $product->save();

        if($data->hasfile('image')){
        	$productid = $product->id;
            foreach($data->file('image') as $file){
                $name=time().$file->getClientOriginalName();
                $file->move(public_path().'/productimage/', $name);  
                $productimage = new ProductimageModel;
                $productimage->name ='/productimage/'. $name;
                $productimage->product_id = $productid  ;
                $productimage->save();
            }
        }
        return redirect('/product');
    }
}
