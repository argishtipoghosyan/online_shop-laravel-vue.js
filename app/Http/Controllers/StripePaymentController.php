<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Stripe;
use App\ProductModel;
use App\UsersModel;
use App\ZambyuxModel;
use App\PatverModel;
use App\ProductimageModel;

class StripePaymentController extends Controller
{
    public function stripe()
    { 
        

        return view('stripe');
    }
      public function stripePost(Request $request)
    {
        $user_id = Session::get('user_id');
        $zambyux = ZambyuxModel::all()->where('user_id',$user_id);
        foreach ($zambyux as $value) {
            $y = ProductModel::where('id',$value->product_id)->first();
            $x = PatverModel::where('product_id',$value->product_id)->first(); 
            if($x!=[]){
                PatverModel::where('product_id',$value->product_id)->update([
                    'qanak' => $x->qanak+$value->qanak
                ]);
            }
            else{
                $patver = new PatverModel;
                $patver->product_id = $value->product_id;
                $patver->qanak = $value->qanak;
                $patver->user_id = $user_id;
                $patver->star = 0;
                $patver->save();
            }
            ProductModel::where('id',$value->product_id)->update([
               'count' => $y->count-$value->qanak
            ]);
        }
        ZambyuxModel::where('user_id',$user_id)->delete();   
        // return redirect('/patver');

        Stripe\Stripe::setApiKey('sk_test_U52mdMFnno9l1ksNHTKoD4mR00Mu69xC5e');
        Stripe\Charge::create ([
                "amount" => Session::get('gin') * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from itsolutionstuff.com." 
        ]);
        Session::forget('gin');
        Session::flash('success', 'Payment successful!');
          
        return redirect('/profile');
    }
}
