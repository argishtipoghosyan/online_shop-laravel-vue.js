<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsersModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Mail;


class SignupController extends Controller
{
    function  signup(){
    	return view('signup');
    }
    function  forgot(){
        return view('forgot');
    }
    function signup_form(Request $data){
	    $validatedUser = $data->validate([
            'Username' => 'required',
            'surname' => 'required',
            'age' => 'required|numeric',
            'email' => 'required|email:rfc,dns|unique:users,email',
            'password' => 'required|min:6',
            'confirmpassword' => 'required|same:password',
        ]);
        $user = new UsersModel;
        $user->name = $data->Username;
        $user->surname = $data->surname;
        $user->age = $data->age;
        $user->email = $data->email;
        $user->password = Hash::make($data->password);
        $user->save();
      
        //send mail
        $name = $data->Username;
        $email = $data->email;
        $hash = md5($email.$user->id);

        Mail::send('signupsendemail', ['name'=>$name,'hash'=>$hash,'email'=>$email], function($message) use ($email) {
            $message->to($email, 'user')->subject('page verification');
            $message->from('poghosyanargishti1998@gmail.com','Admin');
        });
        Session::flash('send_email','An Email has been sent to this email, please reply');
        return redirect('/signup');
    }
    function sendemail($email,$hash){
        $x = UsersModel::where('email',$email)->first();
        if($x!=[]){
           if($hash==md5($x->email.$x->id)){
                UsersModel::where('email',$email)->update([
                    'active' => 1
                ]);
                Session::put('user_id',$x->id);
                return redirect('/profile');
            }
            return redirect('/login');
        }
        return redirect('/login');
    }
    function restore(Request $data){
        $valid = $data->validate([            
            'email1' => 'required|email:rfc,dns',
        ]);
        $x = UsersModel::where('email',$data->email1)->first();
        if($x!=[]){
            Session::flash('ka','uxarkel');
            $name = $x->name;
            $email = $data->email1;
            $hash = rand(1000,9999);
            Mail::send('restore', ['name'=>$name,'hash'=>$hash], function($message) use ($email) {
                $message->to($email, 'user')->subject('Forgot Password');
                $message->from('poghosyanargishti1998@gmail.com','Admin');
            });
            UsersModel::where('email',$email)->update([
                'code' => $hash
            ]);
            Session::put('email',$email);
            Session::flash('res','An Email has been sent to this email, please reply');
            return back();
        }

        else{
            Session::flash('emmail1','No registration');
            return back();
        }
    }
    function confirm(Request $data){
        Session::flash('ka','Wrong code');
        $validatedUser = $data->validate([
            'code' => 'required',
            'password' => 'required|min:6',
            'conpassword' => 'required|same:password',
        ]);
        $email = Session::get('email');
        $x = UsersModel::where('email',$email)->first();
        
        if($x->code==$data->code){
            UsersModel::where('email',$email)->update([
                'code' => $data->code,
                'password' => Hash::make($data->password)
            ]);
            Session::put('user_id',$x->id);
            return redirect('/profile');
        }
        else{
            Session::flash('sxalcode','Wrong code');
            return back();
        }
    }
}