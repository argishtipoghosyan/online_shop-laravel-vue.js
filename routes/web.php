<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });
route::get('/','LoginController@login');
Route::get('/signup','SignupController@signup');
Route::get('/Forgot','SignupController@forgot');
Route::get('/sendemail/{email}/{hash}','SignupController@sendemail');
Route::get('/login','LoginController@login');
route::get('/profile','UserController@profile')->middleware('checkSession');
Route::get('/update','UserController@update')->middleware('checkSession');
Route::get('/product','ProductController@product')->middleware('checkSession');
Route::get('/patver','ProductController@apranq')->middleware('checkSession');
Route::get('/cart','ProductController@cart')->middleware('checkSession');
Route::get('/myproduct','ProductController@myproduct')->middleware('checkSession');
Route::get('/logout','UserController@logout');
Route::get('/product_detail/{id}','ProductController@product_detail');
Route::get('/delete/{id}','ProductController@delete');
Route::get('/admindashboard','AdminController@admin')->middleware('adminSession');

Route::get('/logoutAdmin','AdminController@logoutAdmin');

Route::post('/block','AdminController@block');
Route::post('/allow','AdminController@allowpost');
Route::post('/item','AdminController@itempost');

Route::post('/restore','SignupController@restore');
Route::post('/confirm','SignupController@confirm');
Route::post('/signup_form','SignupController@signup_form');
Route::post('/login_form','LoginController@login_form');
Route::post('/uppas_form','UserController@uppas_form');
Route::post('/up_form','UserController@up_form');
Route::post('/addprod_form','ProductController@addprod_form');
Route::post('/postajax','ProductController@delete');
Route::post('/starajax','ProductController@star');
Route::post('/updat','ProductController@update');
Route::post('/delimg','ProductController@delimg');
Route::post('/jnjel','ProductController@jnjel');
Route::post('/tarmacnel','ProductController@tarmacnel');
Route::post('/zambyux','ProductController@zambyux');



    
Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');